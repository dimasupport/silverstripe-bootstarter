# Silverstripe Composer Install

## Features
- BrowserSync
- jQuery v2.2.4
- Bootstrap v3.3.7
- NPM for front-end package managemnet
- Gulp for SASS and JS Compiling
- Menu loops integrated with Bootstraps navbar and dropdowns
- Merged Bootstrap form styles with SilverStripes form markup
- Responsive tables that output from the CMS (JS wraps all table elements with the table-responsive class)
- Basic SASS folder structure to keep things tidy
- matchHeight for fixing the grid float bug with unequal columns (Check the footer)
- Kitchen sink included on page.ss featuring common bootstrap elements (Just delete what you dont want)
- Supports userforms

## Requirements
1. NodeJS (sudo apt-get install nodejs)
2. NPM (sudo apt-get install npm)
3. NPM legacy (sudo apt-get install nodejs-legacy)
4. Gulp (sudo npm install gulp -g)

## Installation
1. Composer create-project silverstripe/installer /home/dimademo/domains/dimademo.nl/public_html/MAPNAAM 4.0.3 <- VERSION NUMBER
2. Clone or download this respository into your SilverStripe themes directory.
3. Run 'npm install' via cmd line inside the Bootstarter theme folder to get all of the node dependancies, this will also install Gulp for compiling scss and js.
4. Change to the Bootstarter theme in the SilverStripe CMS.
5. Change the PROXY_URL in gulpfile.js in the theme directory to your local development SilverStripe site link for Browsersync.
6. Run "gulp" via the cmd line inside Bootstarter theme folder, this will compile uncompressed sass and js into the /dist/ folder and will also start Browsersync to watch for changes.
7. Run "gulp build" for production, this will minify the scss and js files (you can auto run this from composer *see extras*).
8. Run "composer vendor-expose" in root

## Instructions
- Run "gulp" to watch for scss and javascript changes with Browsersync.
- Run "gulp build" to compile for production, this includes sourcemaps and minification.

## Extras

### Additonal branches
- silverstripe-4 branch (SilverStripe 4 with Bootstrap 4)
- bootstrap-4 branch (SilverStripe 3 with Bootstrap 4)